import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class TiedostoScanner {

	/**
	 * Tiedostoja voi lukea myös scannerilla:
	 * @param args
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException {
		
		String tiedostonimi = args[0]; //otetaan tiedostonimi komentoriviltä
		File tiedosto = new File(tiedostonimi); //File -olio viittaa tähän tiedostoon
		
		Scanner s = new Scanner(tiedosto); //Scanner asetetaan lukemaan tiedostoa, eikä "System.in" -syötevirtaa.
		
		while(s.hasNextLine()) { //jatketaan niin kauan kuin scanner ilmoittaa, että rivejä on vielä jäljellä
			String rivi = s.nextLine();
			System.out.println(rivi);
		}
		s.close();
	}
}
