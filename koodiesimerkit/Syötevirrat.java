import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

public class Syötevirrat {

	/**
	 * Syötevirroilla (inputstream) voidaan lukea tiedostoja juuri niin paljon kerrallaan kuin tarvitsee.
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		File f = new File(args[0]);
		
		FileInputStream fis = new FileInputStream(f);
			
		byte[] puskuri=new byte[100]; //100 tavun puskuri
		int luettu=0;
		
		 //yritetään lukea puskuriin 100 tavua. Metodi tallentaa tiedot puskuriin ja palauttaa todellisuudessa luetun määrän.
		//jatketaan while-silmukkaa, kunnes metodi palauttaa -1 (virta päättyi eikä luettavaa enää ole)
		while((luettu=fis.read(puskuri))!=-1){
			System.out.println("Luettiin " + luettu + " tavua.");
			//muutetaan puskurin sisältä merkkijonoksi, joka sisältää vain tavujen numeeriset arvot.
			String tavut = Arrays.toString(puskuri);
			System.out.println(tavut);
			
			//Tässä voisi myös kokeilla tavujen muuttamista merkkijonoksi, esim new String(puskuri); mutta se ei välttämättä onnistu
			//koska yhtä merkkiä saatetaan esittää useammalla tavulla merkistäkoodauksesta riippuen ja puskuri saattaa katketa keskeltä
			//merkkiä.
			//syötevirtoja käytetään usein muiden tiedostomuotojen kuin tekstien lukemiseen.
		}
		fis.close(); //suljetaan syötevirta.
	}
}
