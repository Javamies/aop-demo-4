import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class Osoitekirja {

	//HUom! Tässä kannattaisi ehkä käyttää listaa, jotta uusia rivejä on helpompi lisätä.
	static String[][] osoitteet;

	public static void main(String[] args) {
		
		
		/*
		 * Try-catch esitellään viimeisellä (huomisella) luennolla.
		 * Try-catch-kontrollirakenteen idea on siis yrittää tehdä jotakin, joka
		 * saattaa heittää poikkeuksen. Jos poikkeus heitetään yrityksen
		 * aikana, otetaan se kiinni ja reagoidaan
		 * tilanteeseen, jotta ohjelman suoritus voi
		 * jatkua normaalisti.
		 */
		try { //yritetään tehdä jotain
			lueOsoitteet();
		} catch (IOException e) { //tapahtui virhe, joka otetaan "kiinni"
			//mitä tehdään kun virhe tapahtui:
			System.out.println("Osoitetiedostoa ei löytynyt!");
			osoitteet=new String[0][3];
			e.printStackTrace();
		}
		
		Scanner s = new Scanner(System.in);
		int valinta=-1;
		do {
			tulostaOhje();
			//TODO: parseInt try-catchiin?
			valinta = Integer.parseInt(s.nextLine());
			switch (valinta) {
			case 1:
				tulostaOsoitteet();
				break;
			case 2:
			//TODO kaikki tulostaOhje -metodissa listatut vaihtoehdot
			default:
				break;
			}
			
		}while(valinta!=4);
		tallennaOsoitteet();
		System.out.println("Osoitteet tallennettu, poistutaan.");
		s.close();
	}



	public static void lueOsoitteet() throws IOException {
		//TODO: pitäisikö tämä saada käyttäjältä?
		File f = new File("osoitteet.csv");
		System.out.println(f.getAbsolutePath());
		List<String> rivit = Files.readAllLines(f.toPath());
		osoitteet = new String[rivit.size()][3];

		for (int i = 0; i < rivit.size(); i++) {
			String[] arvot = rivit.get(i).split(",");
			osoitteet[i] = arvot; // tallennetaan yksiuloitteinen taulukko yhdeksi riviksi kaksiulotteiseen
									// taulukkoon
		}

	}

	/**
	*Luetaan käyttäjältä uusi osoite, joka lisätään osoitekirjaan
	* TODO: parametrit?
	*/
	public static String[] lueUusiOsoite() {
		return null;
		
	}
	/**
	 * Metodi tallentaa osoitekirjan, jotta ohjelman suorituksen aikana lisätyt uudet tiedot ovat käytässä myös ohjelman seuraavalla suorituskerralla
	 */
	public static void tallennaOsoitteet() {
		
		
	}
	public static void tulostaOsoitteet() {
		System.out.println("\nOsoitelista:");
		System.out.println("Nimi\tOsoite\tPuhelinnumero");
		for(String[] rivi : osoitteet) {
			System.out.println(rivi[0]+"\t"+rivi[1]+"\t"+rivi[2]);
		}
		System.out.println("\n");
	}
	
	private static void tulostaOhje() {
		System.out.println("Syötä toiminto:");
		System.out.println("1. Tulosta osoitekirja");
		System.out.println("2. Lisää osoite");
		System.out.println("3. poista osoite");
		System.out.println("4. tallenna ja lopeta");
		
	}
}
